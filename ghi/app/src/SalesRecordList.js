import React from 'react';

class SalesRecordList extends React.Component {
    constructor(props) {
      super(props);
      this.state = { recs: [] };
    }
  
    async componentDidMount() {
      const url = 'http://localhost:8090/salesrecords/';
      const response = await fetch(url);
      
      if (response.ok) {
        const data = await response.json();
      
        this.setState({ recs: data.salesrecord });
        console.log("state.recs");
        console.log(this.state.recs);
      }
    }
  
    render() {
      return (
        <div className="container">
          <h2 className="display-4 fw-bold">Sales Records</h2>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>price</th>
                <th>sold</th>
                <th>vin</th>
                <th>Sales rep</th>
                <th>customer</th>
              </tr>
            </thead>
            <tbody>
              {this.state.recs.map(rec => {
                  return (
                      <tr key={rec.href} className="col" >
                          <td>{rec.price}</td>
                          <td>{rec.sold}</td>
                          <td>{rec.automobile.vin}</td>
                          <td>{rec.saleperson.name}</td>
                          <td>{rec.customer.name}</td>
                      </tr>
                  )
              })}
            </tbody>
          </table>
        </div>
      );
    }
  }
  export default SalesRecordList;