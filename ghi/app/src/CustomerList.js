import React from 'react';

class CustomerList extends React.Component {
    constructor(props) {
      super(props);
      this.state = { cusm: [] };
    }
  
    async componentDidMount() {
      const url = 'http://localhost:8090/customer/';
      const response = await fetch(url);
      
      if (response.ok) {
        const data = await response.json();
      
        this.setState({ cusm: data.customer });
        console.log("state.cusm");
        console.log(this.state.cusm);
      }
    }
  
    render() {
      return (
        <div className="container">
          <h2 className="display-4 fw-bold">Customers</h2>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Phone</th>
              </tr>
            </thead>
            <tbody>
              {this.state.cusm.map(cus => {
                  return (
                      <tr key={cus.href} className="col" >
                          <td>{cus.name}</td>
                          <td>{cus.address}</td>
                          <td>{cus.phone}</td>
                      </tr>
                  )
              })}
            </tbody>
          </table>
        </div>
      );
    }
  }
  export default CustomerList;