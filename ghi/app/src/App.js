import { BrowserRouter, Routes, Route } from 'react-router-dom';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import AutomobilesList from './AutomobilesList';
import MainPage from './MainPage';
import ManufacturerForm from './ManufacturerForm';
import ManufacturersList from './ManufacturersList';
import ModelsList from './ModlesList';
import Nav from './Nav';
import SalesPersonForm from './SalesPersonForm';
import SalesPersonList from './SalesPersonList';
import SalesrecordForm from './SalesRecordForm';
import SalesRecordList from './SalesRecordList';
import SalesRecordFilter from './SalesRecordsFilter';
import VehicleModelForm from './VehicleModelForm';
import AutomobileForm from './AutomobileForm';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentHistory from './AppointmentHistory';

function App(props) {
  // console.log(props);
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salesrecord">
          <Route index element={<SalesRecordList/>}/>
            <Route path="new" element={<SalesrecordForm/>} />
            <Route path="filter" element={<SalesRecordFilter />} />
          </Route>
          <Route path="customer">
            <Route index element={<CustomerList/>}/>
            <Route path="new" element={<CustomerForm/>} />
          </Route>
          <Route path="salesperson">
            <Route index element={<SalesPersonList/>}/>
            <Route path="new" element={<SalesPersonForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelsList />} />
          </Route>
          <Route path="manufacturers" >
            <Route index element={<ManufacturersList/>}/>
            <Route path="new" element={<ManufacturerForm/>} />
          </Route>
          <Route path="models">
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList autos={props.autos} />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
<Route path="new" element={<SalesPersonForm/>} />
export default App;

{/* <Route path="models">
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList autos={props.autos} />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
} */}

