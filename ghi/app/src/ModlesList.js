import React from 'react';

class ModelsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { mods: [] };
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    
    if (response.ok) {
      const data = await response.json();
      console.log(data);
    
      this.setState({ mods: data.models });
      console.log("state.mods");
      console.log(this.state.mods);
    }
  }

  render() {
    return (
      <div className="container">
        <h2 className="display-4 fw-bold">Vehicle Models</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {this.state.mods.map(mod => {
                return (
                    <tr key={mod.href} className="col" >
                        <td>{mod.name}</td>
                        <td>{ mod.manufacturer.name }</td>
                        <td><img src={mod.picture_url}></img></td>
                    </tr>
                )
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ModelsList;