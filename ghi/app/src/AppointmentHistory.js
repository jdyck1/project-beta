import React from 'react';

class AppointmentHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      appts: []
    }
    this.handleChangeVin = this.handleChangeVin.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);
    
    if (response.ok) {
      const data = await response.json();
    
      this.setState({ appts: data.appointments });
    }
  }

  handleChangeVin(event) {
    const value = event.target.value;
    this.setState({ search: value });
  }


  render() {
    return (
      <div className="container">
        <h2 className="display-4 fw-bold">Appointment history</h2>
        <div className="row"></div>
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h2>Enter a VIN</h2>
                <form id="search-for-vin">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeVin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                  </div>
                </form>
            </div>
          </div>
        <table className="table table-striped mt-4">
        <thead>
         <tr>
            <th>VIN</th>
            <th>Customer name</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {this.state.appts.filter(appt => appt.vin === this.state.search).map(filteredAppt => (
              <tr key={filteredAppt.id} className="col">
                <td>{ filteredAppt.vin }</td>
                <td>{ filteredAppt.customer_name }</td>
                <td>{ filteredAppt.appointment_time }</td>
                <td>{ filteredAppt.technician.name }</td>
                <td>{ filteredAppt.appointment_reason }</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    );
  }
}

export default AppointmentHistory;