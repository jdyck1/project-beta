import React from 'react';

class SalesPersonList extends React.Component {
    constructor(props) {
      super(props);
      this.state = { pers: [] };
    }
  
    async componentDidMount() {
      const url = 'http://localhost:8090/salesperson/';
      const response = await fetch(url);
      
      if (response.ok) {
        const data = await response.json();
      
        this.setState({ pers: data.salesperson });
        console.log("state.pers");
        console.log(this.state.pers);
      }
    }
  
    render() {
      return (
        <div className="container">
          <h2 className="display-4 fw-bold">Sales Persons</h2>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Employe Number</th>
              </tr>
            </thead>
            <tbody>
              {this.state.pers.map(per => {
                  return (
                      <tr key={per.href} className="col" >
                          <td>{per.name}</td>
                          <td>{per.number}</td>
                      </tr>
                  )
              })}
            </tbody>
          </table>
        </div>
      );
    }
  }
  export default SalesPersonList;