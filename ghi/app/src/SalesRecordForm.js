import React from 'react';

class SalesrecordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      price: "",
    //   sold: "",
      saleperson_id: "",
      automobile_id: "",
      customer_id: "",
      salerecords: [],
      autos: []
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangePrice = this.handleChangePrice.bind(this);
    // this.handleChangeSold = this.handleChangeSold.bind(this);
    this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
    this.handleChangeAutomobile = this.handleChangeAutomobile.bind(this);
    this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
    this.getAutomobile = this.getAutomobile.bind(this)
  };

  async getAutomobile() {
    const automobileURL = '	http://localhost:8100/api/automobiles/';
    const response = await fetch(automobileURL);
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      this.setState({autos: data.autos});
    }
  }

  async componentDidMount() {
    this.getAutomobile()
    const salesrecordURL = 'http://localhost:8090/salesrecords/';
    const salesrecordresponse = await fetch(salesrecordURL);

    // if (salesrecordresponse.ok) {
    //     const data = await response.json();
    //     console.log(data);
    //     this.setState({ salerecords: data.salerecord })
    //   }
    // }
    if (salesrecordresponse.ok) {
      const salerecorddata = await salesrecordresponse.json();
      console.log(salerecorddata);
      this.setState({salerecords: salerecorddata.salesrecord});
    }
  }

  

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.salerecords;

    const salesrecordUrl = 'http://localhost:8090/salesrecords/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    console.log(data);
    const response = await fetch(salesrecordUrl, fetchConfig);
    if (response.ok) {
      const newSalesRecord = await response.json();
      this.setState({
        price: "",
        // sold: "",
        saleperson_id: "",
        automobile_id: "",
        customer_id: ""
      });
    }
  }


  
    


  handleChangePrice(event) {
    const value = event.target.value;
    this.setState({ price: value });
  }
//   handleChangeSold(event) {
//     const value = event.target.value;
//     this.setState({ sold: value });
//   }
  handleChangeSalesPerson(event) {
    const value = event.target.value;
    this.setState({ saleperson_id: value });
  }
  handleChangeAutomobile(event) {
    const value = event.target.value;
    this.setState({ automobile_id: value });
  }
  handleChangeCustomer(event) {
    const value = event.target.value;
    this.setState({ customer_id: value });
  }
  IsCarSold(vin){
    for (let record of this.state.salerecords){
      console.log(record,"hello i want to ho gome" )
      if (vin === record.automobile.vin){
        return true
      }
    } 
    return false
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
          <h1>create a new sales record</h1>
            <form onSubmit={this.handleSubmit} id="create-vehicle-model-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangePrice} value={this.state.price} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                <label htmlFor="price">price</label>
              </div>
                <div className="mb-3">
                  <select onChange={this.handleChangeSalesPerson} value={this.state.saleperson_id} required name="saleperson_id" id="saleperson_id" className="form-select">
                    <option value="">Choose a employee</option>
                    {this.state.salerecords.map(salerecord => {
                      return (
                        <option key={salerecord.saleperson.id} value={salerecord.saleperson.id}>
                          {salerecord.saleperson.name}
                        </option>
                      )
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleChangeAutomobile} value={this.state.automobile_id} required name="automobile_id" id="automobile_id" className="form-select">
                    <option value="">Choose a automobile</option>
                    {this.state.autos.map(auto => {
                    {/* {this.state.autos.filter(
                     autos => autos.automobile.vin !== autos: [].vin).map(auto => { */}
                      return (! this.IsCarSold(auto.vin)) && (
                        <option key={auto.id} value={auto.id}>
                          {auto.vin}
                        </option>
                      )
                    })}
                  </select>
                </div> 
                <div className="mb-3">
                  <select onChange={this.handleChangeCustomer} value={this.state.customer_id} required name="customer_id" id="customer_id" className="form-select">
                    <option value="">Choose a Customer</option>
                    {this.state.salerecords.map(salerecord => {
                      return (
                        <option key={salerecord.customer.id} value={salerecord.customer.id}>
                          {salerecord.customer.name}
                        </option>
                      )
                    })}
                  </select>
                </div> 
                <button className="btn btn-primary">Create</button> 
            </form>
        </div>
      </div>
    </div>
    );
  }
}

export default SalesrecordForm;

