import React from 'react';

class SalesPersonForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      number: ""
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeNumber = this.handleChangeNumber.bind(this);
  };


  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.salesperson;

    const salespersonUrl = 'http://localhost:8090/salesperson/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    console.log(data);
    const response = await fetch(salespersonUrl, fetchConfig);
    if (response.ok) {
      const newSalesPerson = await response.json();
      this.setState({
        name: "",
        number: ""
      });
    }
  }

  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }
  handleChangeNumber(event) {
    const value = event.target.value;
    this.setState({ number: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add sales person</h1>
            <form onSubmit={this.handleSubmit} id="create-manufacturer-form">
              <div className="form-floating mb-3">   {/* value of the boz set manuly */}
                <input onChange={this.handleChangeName} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeNumber} value={this.state.number} placeholder="Number" required type="text" name="number" id="number" className="form-control" />
                <label htmlFor="number">Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SalesPersonForm;
