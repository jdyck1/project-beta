import React from 'react'


class SalesRecordFilter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      salesperson: [],
      salesrecord: [],
    }
    this.getSalesPerson = this.getSalesPerson.bind(this)
    this.getSalesRecordFilter = this.getSalesRecordFilter.bind(this)
    this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this)
  }

  handleSalesPersonChange(event) {
    const value = event.target.value
    this.setState({saleperson:value})
  }
  

  async getSalesPerson() {
    const salepersonURL = 'http://localhost:8090/salesperson/';
    const salespersonresponse = await fetch(salepersonURL);
    if (salespersonresponse.ok) {
      const data = await salespersonresponse.json();
      console.log(data)
      this.setState({salesperson: data.saleperson});
    }
  }


  async getSalesRecordFilter() {
    const salerecordURL = 'http://localhost:8090/salesrecords/'
    try {
      const salerecordRespnse = await fetch(salerecordURL)
      if (salerecordRespnse.ok) {
        const salerecordData = await salerecordRespnse.json()
        console.log(salerecordData)
        this.setState({
            salesrecord: salerecordData.salerecord,
        })
      }
    } catch (e) {
      console.error(e)
    }
  }

  async componentDidMount() {
    this.getSalesRecordFilter()
    this.getSalesPerson()
  }

  render () {
    return (
        <>
        <div className="form-floating mb-3">
        <select onChange={this.handleSalesPersonChange} value={this.state.saleperson} required name="saleperson" id="saleperson" className="form-select">
            <option value="">Sales Person selection</option>
            {this.state.salesperson.map(saleperson => {
            return (
                <option key={saleperson.id} value={saleperson.id}>
                {saleperson.name}
                </option>
            );
            })}
            </select>
       </div>
      <table className="table table-striped table-hover table-bordered">
      <caption>List of sale records</caption>
      <thead className="table-dark">
        <tr>
          <th>Sale Person</th>
          <th>Customer</th>
          <th>Price</th>
          <th>VIN</th>
        </tr>
      </thead>
      <tbody>
       {this.state.salesrecord.filter(
        salerecord => salerecord.saleperson.id.toString() === this.state.saleperson.id).map(salerecord => {
        return (
          <tr key={salerecord.id}>
            <td>{salerecord.saleperson.name}</td>
            <td>{salerecord.customer.name}</td> 
            <td>{salerecord.price}</td>
            <td>{salerecord.automobile.vin}</td>
          </tr>
        )
       })
       }
      </tbody>
    </table>
    </>
    )
  }
}
export default SalesRecordFilter
