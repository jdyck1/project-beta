import React from 'react';

class AppointmentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointment_status: "",
      appts: [],
      autos: []
    }
    // this.handleFinish = this.handleFinish.bind(this);
    // this.handleCancel = this.handleCancel.bind(this);
  }

  async componentDidMount() {
    const apptUrl = 'http://localhost:8080/api/appointments/';
    const apptResponse = await fetch(apptUrl);

    const autoUrl = 'http://localhost:8100/api/automobiles/';
    const autoResponse = await fetch(autoUrl);

    if (apptResponse.ok) {
      const apptData = await apptResponse.json();
      console.log("appt");
      console.log(apptData);

      this.setState({ appts: apptData.appointments });
    }
    
    if (autoResponse.ok) {
        const autoData = await autoResponse.json();
        console.log("auto");
        console.log(autoData);

        this.setState({ autos: autoData.autos });
    }
  }

//   handlers for something that doesn't work
  /*
  async handleFinish() {
    this.setState({ appointment_status: "FINISHED" });
    const data = {...this.state};
    delete data.appts;
    delete data.autos;

    const apptUrl = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(apptUrl, fetchConfig);
    if (response.ok) {
      const newAppt = await response.json();
      this.setState({
        appointment_status: "",
      });
    }
  }
  */

//   async handleCancel() {
//     this.setState({ appointment_status: "CANCELED" });
//   }

  vinTableData(vin) {
    for (let auto of this.state.autos) {
      if (auto.vin === vin) {
        return <td style={{color: 'green'}}>VIP { vin }</td>;  
      } 
    }
    return <td>{ vin }</td>;
  }

  render () {
    return (
      <div className="container">
        <h2 className="display-4 fw-bold">Active appointments</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer name</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {this.state.appts.filter(appt => appt.appointment_status === "IN PROGRESS").map(filteredAppt => (
              <tr key={filteredAppt.id} className="col">
                { this.vinTableData(filteredAppt.vin) }
                <td>{ filteredAppt.customer_name }</td>
                <td>{ filteredAppt.appointment_time }</td>
                <td>{ filteredAppt.technician.name }</td>
                <td>{ filteredAppt.appointment_reason }</td>
                {/* these buttons don't work */}
                {/* <button onClick={this.handleFinish} className="btn">Finish</button>
                <button onClick={this.handleCancel} className="btn">Cancel</button> */}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default AppointmentList;