import React from 'react';

class AppointmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin: "",
      customer_name: "",
      appointment_time: "",
      appointment_reason: "",
      appointment_status: "",
      technician_id: "",
      techs: []
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeVin = this.handleChangeVin.bind(this);
    this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
    this.handleChangeTime = this.handleChangeTime.bind(this);
    this.handleChangeReason = this.handleChangeReason.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
    this.handleChangeTech = this.handleChangeTech.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      this.setState({ techs: data.technicians })
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.techs;

    const appointmentUrl = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    console.log(data);
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      this.setState({
        vin: "",
        customer_name: "",
        appointment_time: "",
        appointment_reason: "",
        appointment_status: "",
        technician_id: ""
      });
    }
  }

  handleChangeVin(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  handleChangeCustomer(event) {
    const value = event.target.value;
    this.setState({ customer_name: value });
  }

  
  handleChangeTime(event) {
    const value = event.target.value;
    this.setState({ appointment_time: value });
  }
    
  handleChangeReason(event) {
    const value = event.target.value;
    this.setState({ appointment_reason: value });
  }

  handleChangeStatus(event) {
    const value = event.target.value;
    this.setState({ appointment_status: value });
  }

  handleChangeTech(event) {
    const value = event.target.value;
    this.setState({ technician_id: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a service appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeVin} value={this.state.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeCustomer} value={this.state.customer_name} placeholder="CustomerName" required type="text" name="customer_name" id="customer_name" className="form-control" />
                <label htmlFor="customer_name">Customer name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeTime} value={this.state.appointment_time} placeholder="ApptTime" required type="datetime-local" name="appointment_time" id="appointment_time" className="form-control" />
                <label htmlFor="appointment_time">Appointment time</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={this.handleChangeReason} value={this.state.appointment_reason} placeholder="ApptReason" required type="text" name="appointment_reason" id="appointment_reason" className="form-control" />
                <label htmlFor="appointment_reason">Appointment reason</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeStatus} value={this.state.appointment_status} required name="appointment_status" id="appointment_status" className="form-select">
                  <option value="">Appointment status</option>
                  <option value="IN PROGRESS">IN PROGRESS</option>
                  <option value="FINISHED">FINISHED</option>
                  <option value="CANCELED">CANCELED</option>
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeTech} value={this.state.technician_id} required name="technician_id" id="technician_id" className="form-select">
                  <option value="">Choose a technician</option>
                  {this.state.techs.map(tech => {
                    return (
                      <option key={tech.id} value={tech.id}>
                        {tech.name}
                      </option>
                    )
                  })}
                </select>
              </div>
            <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

}

export default AppointmentForm