import React from 'react';

class ManufacturersList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { mans: [] };
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    
    if (response.ok) {
      const data = await response.json();
    
      this.setState({ mans: data.manufacturers });
      console.log("state.mans");
      console.log(this.state.mans);
    }
  }

  render() {
    return (
      <div className="container">
        <h2 className="display-4 fw-bold">Manufacturers</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {this.state.mans.map(man => {
                return (
                    <tr key={man.href} className="col" >
                        <td>{man.name}</td>
                    </tr>
                )
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ManufacturersList;