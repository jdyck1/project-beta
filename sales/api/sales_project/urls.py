"""sales_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from sales_rest.views import api_customer,api_customerdeets,api_salesperson, api_salespersondeets, api_salesrecordlist, api_salesrecorddeetd

urlpatterns = [
    path('admin/', admin.site.urls),
    #path('salesrecord/', api_list_salerecord, name="list_salesrecord"),
    path("customer/", api_customer , name="create_customer"),
    path("customer/<int:pk>/",api_customerdeets,name="api_automobile",
    ),
    path("salesperson/",api_salesperson,name="create_salesperson"),
    path("salesperson/<str:number>/",api_salespersondeets,name="sales_person_deets"),
    path("salesrecords/",api_salesrecordlist,name="list_sale_records"),
    path("salesrecords/<str:saleperson>/",api_salesrecorddeetd,name="sales_person_deets"),

]
