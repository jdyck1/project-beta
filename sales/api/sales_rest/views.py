from json import encoder
#from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http  import require_http_methods 
import json
from common.json import ModelEncoder
from .models import Customer, SalesPerson, AutomobileVO, SalesRecord

class AutomobileVODetailEncoder (ModelEncoder): 
    model = AutomobileVO  
    properties = ["vin", "id"] 

class SalesPersonDeets(ModelEncoder): 
    model = SalesPerson 
    properties = [
        "name",
        "number",
        "id"
        ]
class CustomerDeetailEncoder(ModelEncoder): 
    model = Customer 
    properties = [
        "name",
        "address",
        "phone",
        "id"
        ]
class SaleRecordDetailEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "price",
        "sold",
        "automobile",
        "saleperson",
        "automobile",
        "customer"
    ]
    encoders = {
        "saleperson": SalesPersonDeets(), 
        "customer": CustomerDeetailEncoder(),
        "automobile": AutomobileVODetailEncoder(), 
    }
 
@require_http_methods(["GET"])
def api_salesrecorddeetd(request,saleperson):
    if request.method == "GET":
        salerecords= SalesRecord.objects.filter(saleperson=saleperson) 
        return JsonResponse(
            {"salerecords": salerecords},
            encoder= SaleRecordDetailEncoder, 
        )
 
@require_http_methods(["GET", "POST"])
def api_salesrecordlist(request):
    if request.method == "GET":
        try:         
            salesrecord = SalesRecord.objects.all()
            return JsonResponse(
                {"salesrecord": salesrecord},
                encoder = SaleRecordDetailEncoder,
            )
        except:
            response = JsonResponse(
                {"message": "no sales record in data base"}
            )
            response.status_code = 400
            return response
    else: 
        content = json.loads(request.body)
        salesrecord = SalesRecord.objects.create(**content)  
        return JsonResponse(
            salesrecord,
            encoder=SaleRecordDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_salesperson(request):
    if request.method == "GET":
        try:         
            salesperson = SalesPerson.objects.all()
            return JsonResponse(
                {"salesperson": salesperson},
                encoder= SalesPersonDeets,
            )
        except:
            response = JsonResponse(
                {"message": "no salesperson in data base"}
            )
            response.status_code = 400
            return response
    else: 
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)  
            return JsonResponse(
                salesperson,
                encoder=SalesPersonDeets,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_salespersondeets(request,number):
    if request.method == "GET":
        try:
            salesperson = SalesPerson.objects.get(number=number)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonDeets,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesperson = SalesPerson.objects.get(number=number)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalesPersonDeets,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.get(number=number)

            props = ["name", "numberber"]
            for prop in props:
                if prop in content:
                    setattr(salesperson, prop, content[prop])
            salesperson.save()
            return JsonResponse(
                salesperson,
                encoder=SalesPersonDeets,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
 

@require_http_methods(["DELETE", "GET", "PUT"])
def api_customerdeets(request,pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerDeetailEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerDeetailEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerDeetailEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        try:        
            customer = Customer.objects.all()
            return JsonResponse(
                {"customer": customer},
                encoder= CustomerDeetailEncoder,
            )
        except:
            response = JsonResponse(
                {"message": "no customers in data base"}
            )
            response.status_code = 400
            return response

    else: 
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)  
            return JsonResponse(
                customer,
                encoder=CustomerDeetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response



