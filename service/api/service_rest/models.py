from django.db import models


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return str(self.pk)


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=200)
    appointment_time = models.DateTimeField()
    appointment_reason = models.TextField()
    appointment_status = models.CharField(max_length=100, default="IN PROGRESS")

    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.vin

