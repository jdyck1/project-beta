from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import TechnicianEncoder, AppointmentEncoder
from .models import Technician, Appointment


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        try:
            techs = Technician.objects.all()
            return JsonResponse(
                {"technicians": techs},
                encoder=TechnicianEncoder,
            )
        except:
            response = JsonResponse(
                {"message": "No technicians"}
            )
            response.status_code = 400
            return response
    else:
        try:
            content = json.loads(request.body)
            tech = Technician.objects.create(**content)
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create the technician"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            tech = Technician.objects.get(id=pk)
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            tech = Technician.objects.get(id=pk)
            tech.delete()
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            tech = Technician.objects.get(id=pk)

            props = ["name", "employee_number"]
            for prop in props:
                if prop in content: 
                    setattr(tech, prop, content[prop])
            tech.save()
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        try:
            appts = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appts},
                encoder=AppointmentEncoder,
            )
        except:
            response = JsonResponse(
                {"message": "No appointments"}
            )
            response.status_code = 400
            return response
    else:
        try:
            content = json.loads(request.body)
            appt = Appointment.objects.create(**content)
            return JsonResponse(
                appt,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create the appointment"})
            response.status_code = 400
            return response
    

@require_http_methods(["DELETE", "GET", "PUT"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            appt = Appointment.objects.get(pk=pk)
            return JsonResponse(
                appt,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appt = Appointment.objects.get(pk=pk)
            appt.delete()
            return JsonResponse(
                appt,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})
    else:
        try:
            content = json.loads(request.body)
            appt = Appointment.objects.get(pk=pk)

            props = [
                "vin", 
                "customer_name", 
                "appointment_time",
                "appointment_reason",
                "appointment_status",
                "technician",
            ]
            for prop in props:
                if prop in content:
                    setattr(appt, prop, content[prop])
            appt.save()
            return JsonResponse(
                appt,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response