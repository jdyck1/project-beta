from common.json import ModelEncoder
from .models import Technician, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "appointment_time",
        "appointment_reason",
        "appointment_status",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


