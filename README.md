# CarCar

Team:

* Jeremiah - Service
* Austin - Sales

## Design
we have a link to a excalidraw where https://excalidraw.com/#room=5df9176148369d86e80a,VG7AqMZGegKx_91xm4s5gw we broke the API down into models views and URLs using those to then break it down further. as a team our goal was to work out exactly what our responsibilities are. once that was accomplished it was time to divide and conquer. Jeremiah took on the services and I took on sales. to maximize our productivity he dove head first into the front end and I dove into the back end. we did this to optimize time at the end if we both could run into as many different errors as we could in the beginning it would make the teamwork and workflow far more cohesive in the long run. this paid off in the end I was able to help Jeremiah with his API and Jeremiah was able to help me through my Front end. in the end, we were very satisfied with the code we put out. below we will break down our individual responsibilities as well as how we approached them.


## Service microservice (Jeremiah)
I have a technician model with a name and unique employee number. I also have an appointment model with a foreignkey to the technician so you can represent the technician by its id until you need the data within its
instance. The appointment model also has fields for vin, customer name, and appointment time, reason, and status. I did most of my thinking in the model I made in the excalidraw shared by Austin and me.

The problems I needed to solve for my microservice were as follows: 
Create a service appointment, create a technician, and list appointments a couple of ways. Creating an appointment and technician was straightforward.

The first appointment list page needed to list appointments only if status was "in progress" and to indicate if an appointment belonged to a "VIP" customer. Someone was a VIP if they bought a car from the dealership, and a car was bought from the dealership if the car's VIN still existed in the inventory (because cars that are purchased don't get removed from the inventory). "Cancel" and "finish" buttons were also needed for that page but in the time I had left, I couldn't figure out how to make them work so I ended up commenting out every bit of code associated with that part of the page. To fulfill the rest of the requirements for this page, I thought that doing all of the "thinking" in the frontend would be the most efficient way of solving the problems at hand, so I brought in the data from the automobile inventory and I took the data from the appointments API and I compared VINs in each appointment object instance to every VIN in automobiles, and if there was a match the VIN would display "VIP" next to it and the text would be green instead of black.

The second appointment list page needed to list appointments if their VIN matched the input in the search bar. I thought that a single text field that would constantly update would be an easy way of allowing a "search" without actually having to learn to make a real search bar, so that's what I did and it got the job done.

## Sales microservice (Austin)
I worked on the sales API building the models, poller, views, and URLs I got all of my models designed first and started with building the GET and POST for a said model then the GET PUT and Delete as needed once I had those built and linked out in the URLs and functioning on insomnia it was time to make a front end. starting with the front end. if you take a look at the branch named austin you can get a more indepth look at all of the code i indiviulay wrote below is a small list as well.
sales (API,common,sales_project,sales_rest,poll)
app.scr(Create an automobile in inventory, Show a list of automobiles in inventory,Create a vehicle model)

